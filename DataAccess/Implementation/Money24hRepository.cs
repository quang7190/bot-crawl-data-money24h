﻿using DataAccess.Repositories;
using Models.BaseModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Implementation
{
    public class Money24hRepository : BaseRepository,IMoney24hRepository
    {
        public Money24hRepository(IDbTransaction transaction)
            : base(transaction)
        {

        }
        public async Task<int> Import(InterestSaveViewModel model)
        {
            try
            {
                int result = await ExecuteAsync("SPA_Insert_Money24h_Interest_Save_Result", new
                {
                    @date  = model.Date,
                    @result = model.Result,
                    @deposits = model.Deposits,
                    @bankId = model.BankId,
                    @monthId = model.MonthId
                });
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
