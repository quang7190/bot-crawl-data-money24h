﻿using Models.BaseModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public interface IMoney24hRepository
    {
        Task<int> Import(InterestSaveViewModel model);
    }
}
