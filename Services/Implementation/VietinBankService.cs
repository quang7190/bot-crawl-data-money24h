﻿using DataAccess;
using HtmlAgilityPack;
using Infrastructure;
using Models.BaseModels;
using Models.ViewModels;
using Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Services.Implementation
{
    public class VietinBankService : IVietinBankService
    {
        private readonly IApplicationSettings _appSettings;
        private readonly string _connectString;

        public VietinBankService(IApplicationSettings appSettings)
        {
            _appSettings = appSettings;
            _connectString = _appSettings.ConnectionString;
        }
        #region vietinbank
        public async Task<Result> GetVietinResult()
        {
            try
            {
                var url = "https://www.vietinbank.vn/ss/Satellite;jsessionid=1PZUASoEDgG6hkqIKOluRZabtG2xRDSlfPTaz2R-0XwlJHJRcRss!-691499384?c=Page&cid=1456845014284&pagename=vietinbank.vn%2FPage%2FTrangThanhPhan2CotVTBVNLayout&rendermode=preview";

                HtmlWeb web = new HtmlWeb();
                var client = new HttpClient();
                HtmlDocument htmlDoc = new HtmlDocument();
                var response = await client.GetAsync(url);
                if (!response.IsSuccessStatusCode)
                {
                    Console.WriteLine("Lỗi ");
                }
                var body = await response.Content.ReadAsStringAsync();
                Result model = new Result();
                htmlDoc.LoadHtml(body);
                var table = htmlDoc.DocumentNode.SelectNodes("//table[@id='hor-ex-b']");

                //var result = table[0].ChildNodes[4].ChildNodes[1].InnerHtml;
                model.Interest.Add(table[0].ChildNodes[4].ChildNodes[1].InnerHtml);
                model.Interest.Add(table[0].ChildNodes[6].ChildNodes[1].InnerHtml);
                model.Interest.Add(table[0].ChildNodes[8].ChildNodes[1].InnerHtml);
                model.Interest.Add(table[0].ChildNodes[11].ChildNodes[1].InnerHtml);
                model.Interest.Add(table[0].ChildNodes[14].ChildNodes[1].InnerHtml);
                model.Interest.Add(table[0].ChildNodes[17].ChildNodes[1].InnerHtml);
                model.Interest.Add(table[0].ChildNodes[18].ChildNodes[1].InnerHtml);
                model.Interest.Add(table[0].ChildNodes[19].ChildNodes[1].InnerHtml);
                model.Interest.Add(table[0].ChildNodes[20].ChildNodes[1].InnerHtml);

                return model;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<int> InsertVietinBank(Result resultInteres)
        {
            try
            {
                using (UnitOfWork uow = new UnitOfWork(_connectString))
                {
                    int result = 0;

                    int index = 1;
                    for (int i = 0; i < resultInteres.Interest.Count; i++)
                    { 
                        var model = new InterestSaveViewModel
                        {
                            Date = DateTime.Now,
                            Result = resultInteres.Interest[i],
                            Deposits = null,
                            BankId = 2,
                            MonthId = i+1
                        };
                        result = await uow.money24hRepo.Import(model);
                        index++;
                    }
                    uow.Commit();
                    return result;
                }
            }

            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion 

        #region agribank
        public async Task<Result> GetAgriResult()
        {
            try
            {
                var url = "https://www.agribank.com.vn/vn/lai-suat";

                HtmlWeb web = new HtmlWeb();
                var client = new HttpClient();
                HtmlDocument htmlDoc = new HtmlDocument();
                var response = await client.GetAsync(url);
                if (!response.IsSuccessStatusCode)
                {
                    Console.WriteLine("Lỗi ");
                }
                var body = await response.Content.ReadAsStringAsync();
                Result model = new Result();
                htmlDoc.LoadHtml(body);

                var section = htmlDoc.DocumentNode.SelectNodes("//section[@class='bl_cctinhtoan width_common']");

                var table = section[0].ChildNodes[3].ChildNodes[3].ChildNodes[1];
                var tbody = table.ChildNodes[3];

                model.Interest.Add(tbody.ChildNodes[1].ChildNodes[3].InnerHtml.Replace("%","0"));
                model.Interest.Add(tbody.ChildNodes[3].ChildNodes[3].InnerHtml.Replace("%", "0"));
                model.Interest.Add(tbody.ChildNodes[7].ChildNodes[3].InnerHtml.Replace("%", "0"));
                model.Interest.Add(tbody.ChildNodes[13].ChildNodes[3].InnerHtml.Replace("%", "0"));
                model.Interest.Add(tbody.ChildNodes[19].ChildNodes[3].InnerHtml.Replace("%", "0"));
                model.Interest.Add(tbody.ChildNodes[25].ChildNodes[3].InnerHtml.Replace("%", "0"));
                model.Interest.Add(tbody.ChildNodes[27].ChildNodes[3].InnerHtml.Replace("%", "0"));
                model.Interest.Add(tbody.ChildNodes[31].ChildNodes[3].InnerHtml.Replace("%", "0"));
                model.Interest.Add(tbody.ChildNodes[33].ChildNodes[3].InnerHtml.Replace("%", "0"));

                return model;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<int> InsertAgriBank(Result resultInteres)
        {
            try
            {
                using (UnitOfWork uow = new UnitOfWork(_connectString))
                {
                    int result = 0;

                    int index = 1;
                    for (int i = 0; i < resultInteres.Interest.Count; i++)
                    {
                        var model = new InterestSaveViewModel
                        {
                            Date = DateTime.Now,
                            Result = resultInteres.Interest[i],
                            Deposits = null,
                            BankId = 4,
                            MonthId = i + 1
                        };
                        result = await uow.money24hRepo.Import(model);
                        index++;
                    }
                    uow.Commit();
                    return result;
                }
            }

            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion 
    }
}
