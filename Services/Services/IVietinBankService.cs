﻿using Models.BaseModels;
using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Services
{
    public interface IVietinBankService
    {
        Task<Result> GetVietinResult();
        Task<int> InsertVietinBank(Result model);
        Task<Result> GetAgriResult();
        Task<int> InsertAgriBank(Result model);
    }
}
