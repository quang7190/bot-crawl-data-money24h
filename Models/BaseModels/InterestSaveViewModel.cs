﻿using Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.BaseModels
{
    public class InterestSaveViewModel
    {
        public DateTime Date { get; set; }
        public string Result { get; set; }
        public long? Deposits { get; set; }
        public int BankId { get; set; }
        public int MonthId { get; set; }
    }
}
