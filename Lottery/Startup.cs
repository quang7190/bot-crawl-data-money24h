using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using DataAccess;
using Infrastructure;
using Lottery.JobScheduler;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Quartz;
using Quartz.Impl;
using Services.Implementation;
using Services.Services;
using static Dapper.SqlMapper;

namespace Lottery
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IApplicationSettings, ApplicationSettings>();


            services.AddScoped<IVietinBankService, VietinBankService>();
            services.AddControllers();
            ApplicationSettingsFactory.InitializeApplicationSettings(new ApplicationSettings(Configuration));
            services.AddScoped<SimpleJob>();
            services.AddScoped<SimpleJob2>();
            services.AddSingleton<IScheduler>(ConfigureQuartz(services));
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1",
                    new Info
                    {
                        Title = "My API - V1",
                        Version = "v1"
                    }
                 );

                //var filePath = Path.Combine(System.AppContext.BaseDirectory, "MyApi.xml");
                //c.IncludeXmlComments(filePath);
            });

            services.AddRouting(options => options.LowercaseUrls = true);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });
        }

        public IScheduler ConfigureQuartz(IServiceCollection services)
        {
            NameValueCollection props = new NameValueCollection()
            {
                ["quartz.serializer.type"] = "binary"
            };
            StdSchedulerFactory factory = new StdSchedulerFactory(props);
            var scheduler = factory.GetScheduler().Result;
            new JobSchedulerConfig(scheduler).InitializeJob();
            scheduler.JobFactory = new LotteryJobFactory(services.BuildServiceProvider());
            scheduler.Start().Wait();
            return scheduler;
        }
    }

    public static class ApplicationSettingsFactory
    {
        private static IApplicationSettings _iApplicationSetting;

        public static void InitializeApplicationSettings(IApplicationSettings iApplicationSettings)
        {

            _iApplicationSetting = iApplicationSettings;
            using (UnitOfWork uow = new UnitOfWork(iApplicationSettings.ConnectionString))
            {
                
                Dictionary<string, int> prizesKeyPairValue = new Dictionary<string, int>();
                Dictionary<string, int> lotteryTypesKeyPairValue = new Dictionary<string, int>();
                Dictionary<string, string> lotterySchedulesKeyPairValue = new Dictionary<string, string>();
                

                uow.Commit();
            }
        }
        public static IApplicationSettings GetApplicationSettings()
        {
            return _iApplicationSetting;
        }
    }
}
