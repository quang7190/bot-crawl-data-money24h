﻿using Microsoft.AspNetCore.Mvc;
using Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lottery.Controllers
{
    public class Money24hController : ControllerBase
    {
        private readonly IVietinBankService _vietinBankService;

        public Money24hController(IVietinBankService vietinBankService)
        {
            _vietinBankService = vietinBankService;
        }

        [HttpGet("vietin-bank")]
        public async Task<IActionResult> GetVietinBankResult()
        {
            try
            {
                var resultPrize = await _vietinBankService.GetVietinResult();
                var result = await _vietinBankService.InsertVietinBank(resultPrize);
                return Ok(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet("agri-bank")]
        public async Task<IActionResult> GetAgriBankResult()
        {
            try
            {
                var resultPrize = await _vietinBankService.GetAgriResult();
                var result = await _vietinBankService.InsertAgriBank(resultPrize);
                return Ok(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
