﻿using System;
using System.Threading.Tasks;
using Quartz;

namespace Lottery.JobScheduler
{
    public class SimpleJob2 : IJob
    {
        public SimpleJob2()
        {
        }
        public async Task Execute(IJobExecutionContext context)
        {
            Console.WriteLine($"{DateTime.Now:hh:mm:ss dd/MM/yyyy} Alo Alo Alo Alo");
        }
    }
}
