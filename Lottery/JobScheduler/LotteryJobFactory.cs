﻿using System;
using Quartz;
using Quartz.Spi;

namespace Lottery.JobScheduler
{
    public class LotteryJobFactory : IJobFactory
    {
        private readonly IServiceProvider _serviceProvider;
        public LotteryJobFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
        {
            Console.WriteLine($"{bundle.JobDetail.Key} - {bundle.JobDetail.JobType}");
            var job = _serviceProvider.GetService(bundle.JobDetail.JobType) as IJob;
            return job;
        }

        public void ReturnJob(IJob job)
        {
            var disposable = job as IDisposable;
            disposable?.Dispose();
        }
    }
}
