﻿using System;
using System.Collections.Generic;
using Quartz;

namespace Lottery.JobScheduler
{
    public class JobSchedulerConfig
    {
        private readonly IScheduler _scheduler;
        private readonly Dictionary<string, string> _cronEx;

        public JobSchedulerConfig(IScheduler scheduler)
        {
            _scheduler = scheduler;
            
        }

        public void InitializeJob()
        {
            
         }

        

        private void AddSimpleJob2()
        {
            IJobDetail job = JobBuilder.Create<SimpleJob2>()
                .WithIdentity("SimpleJob2", "Lottery")
                .Build();
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("SimpleJobTrigger2", "JobTrigger")
                .StartNow()
                .WithCronSchedule("0/10 * * ? * * *")
                .Build();
            var x = _scheduler.ScheduleJob(job, trigger).Result;
        }
    }
}
